import api from "./api";

export const getTodoTask = () => {
    return api.get('/todo')
}

export const updateTodoTask = (id, todoItem) => {
    return api.put(`/todo/${id}`, todoItem)
}

export const deleteTodoTask = (id) => {
    return api.delete(`/todo/${id}`)
}

export const addTodoTask = (todoItem) => {
    return api.post(`/todo/`, todoItem)
}

export const getTodoById = (id) => {
    return api.get(`/todo/${id}`)
}