import {initTodoTask} from "../todo/reducers/todoSlice";
import * as todoService from "../../apis/todo"
import {useDispatch} from "react-redux";

export const useTodos = () => {
    const dispatch = useDispatch()
    const loadTodos = async () => {
        const response = await todoService.getTodoTask()
        dispatch(initTodoTask(response?.data))
    }

    const getTodoById=async (id)=>{
        return await todoService.getTodoById(id)
    }

    const updateTodoStatus = async (task) => {
        await todoService.updateTodoTask(task.id, {done: !task.done})
        await loadTodos()
    }

    const updateTaskName = async (task,newTaskName) => {
        await todoService.updateTodoTask(task.id, {name: newTaskName})
        await loadTodos()
    }

    const deleteTodo = async (task) => {
        await todoService.deleteTodoTask(task.id)
        await loadTodos()
    }

    const addTodo = async (taskName) => {
        await todoService.addTodoTask({name: taskName, done: false})
        await loadTodos()
    }

    return {
        loadTodos,
        getTodoById,
        updateTodoStatus,
        updateTaskName,
        deleteTodo,
        addTodo
    }
}