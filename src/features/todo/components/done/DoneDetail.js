import {useParams} from "react-router-dom";
import {useSelector} from "react-redux";

export default function DoneDetail() {
    const {id} = useParams()
    const doneTasks = useSelector(state => state.todo.tasks.find(task => task.id === id))
    return (
        <div className='done-detail'>
            <h1>done detail</h1>
            <div>{doneTasks?.id}</div>
            <div>{doneTasks?.name}</div>
        </div>
    );
}