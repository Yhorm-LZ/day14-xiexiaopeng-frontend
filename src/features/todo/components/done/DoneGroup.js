import {useSelector} from 'react-redux'
import DoneItem from "./DoneItem";

export default function DoneGroup() {
    const doneTasks = useSelector(state => state.todo.tasks.filter(todoTask => todoTask.done === true))
    return (
        <div className='todo-group'>
            {doneTasks.map(((doneTask) =>
                    <DoneItem key={doneTask.id} task={doneTask}></DoneItem>
            ))}
        </div>
    );
}