import {useNavigate} from "react-router-dom";
import {useTodos} from "../../../hooks/useTodo";
import {CloseOutlined} from "@ant-design/icons";

export default function DoneItem(props) {
    const {deleteTodo} = useTodos()
    const navigate = useNavigate()
    const handleTaskNameClick = () => {
        navigate('/done/' + props.task.id)
    }
    const handleRemoveButtonClick = async () => {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            await deleteTodo(props.task)
        }
    }
    return (
        <div className='todo-item'>
            <del onClick={handleTaskNameClick}>{props.task.name}</del>
            <div className='remove-button' onClick={handleRemoveButtonClick}>
                <CloseOutlined/>
            </div>
        </div>
    );
}