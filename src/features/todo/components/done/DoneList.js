import DoneGroup from "./DoneGroup";

export default function DoneList() {
    return (
        <div>
            <h1>Done List</h1>
            <DoneGroup/>
        </div>
    );
}