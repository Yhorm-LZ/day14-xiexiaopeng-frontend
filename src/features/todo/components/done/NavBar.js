import {Breadcrumb} from "antd";
import {NavLink} from "react-router-dom";

export default function NavBar() {
    return (
        <div className="ant-breadcrumb">
            <Breadcrumb
                items={[
                    {
                        title: <NavLink to={'/'}>Home</NavLink>
                    },
                    {
                        title: <NavLink to={'/done'}>Done List</NavLink>,
                    },
                    {
                        title: <NavLink to={'/help'}>Help</NavLink>,
                    }
                ]}
            />
        </div>
    );
}