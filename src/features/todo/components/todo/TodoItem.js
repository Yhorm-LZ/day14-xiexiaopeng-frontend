import {useTodos} from "../../../hooks/useTodo";
import {CloseOutlined, EditOutlined, FundViewOutlined} from "@ant-design/icons";
import {Button, Modal} from 'antd';
import {useState} from "react";
import TextArea from "antd/es/input/TextArea";

export default function TodoItem({task}) {
    const [isEditModalOpen, setIsEditModalOpen] = useState(false);
    const [isViewModalOpen, setIsViewModalOpen] = useState(false);
    const [newTaskName, setNewTaskName] = useState("");
    const [taskData, setTaskData] = useState({});
    const {updateTodoStatus, deleteTodo, updateTaskName, getTodoById} = useTodos()
    const handleTaskNameClick = async () => {
        await updateTodoStatus(task)
    }

    const handleRemoveButtonClick = async () => {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            await deleteTodo(task)
        }
    }

    const handleNewTaskNameChange = async (e) => {
        const value = e.target.value;
        setNewTaskName(value);
    }

    const handleUpdateButtonClick = async () => {
        setNewTaskName(task.name);
        setIsEditModalOpen(true);
    }

    const handleEditOk = async () => {
        await updateTaskName(task, newTaskName)
        setIsEditModalOpen(false);
        setNewTaskName('')
    }

    const handleEditCancel = async () => {
        setIsEditModalOpen(false);
        setNewTaskName(task.name)
    }

    const handleViewOk = async () => {
        setIsViewModalOpen(false);
    }

    const handleViewCancel = async () => {
        setIsViewModalOpen(false);
    }
    const handleViewButtonClick = async () => {
        const {data} = await getTodoById(task.id)
        setTaskData(data)
        setIsViewModalOpen(true);
    }

    return (
        <div className='todo-item'>
            <div className={`task-name ${task.done ? 'done' : ''}`} onClick={handleTaskNameClick}>
                {task.name}
            </div>
            <div className='buttons'>
                <div>
                    <FundViewOutlined onClick={handleViewButtonClick}/>
                </div>
                <div>
                    <EditOutlined onClick={handleUpdateButtonClick}/>
                </div>
                <div className='remove-button' onClick={handleRemoveButtonClick}>
                    <CloseOutlined/>
                </div>
            </div>
            <Modal title="Edit Todo" open={isEditModalOpen} onOk={handleEditOk} onCancel={handleEditCancel}>
                <TextArea value={newTaskName} onChange={handleNewTaskNameChange} rows={4}
                          placeholder="Please input the content."/>
            </Modal>
            <Modal title="View Todo" open={isViewModalOpen} onCancel={handleViewCancel}
                   footer={[
                <Button type={"primary"} size={"large"} key="back" onClick={handleViewOk}>
                    Ok
                </Button>]}>
                <div className='task-info'>
                    <div>
                        id:<span>{taskData.id}</span>
                    </div>
                    <div>
                        name:<span>{taskData.name}</span>
                    </div>
                    <div>
                        <span>{taskData.done ? 'done' : 'not done'}</span>
                    </div>
                </div>
            </Modal>
        </div>
    );
}