import {createSlice} from '@reduxjs/toolkit'

export const todoSlice = createSlice({
    name: 'todo',
    initialState: {
        tasks: []
    },
    reducers: {
        initTodoTask: (state, action) => {
            state.tasks = [...action.payload]
        }
    }
})

export const {initTodoTask} = todoSlice.actions
export default todoSlice.reducer