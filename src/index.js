import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import store from './store'
import {Provider} from 'react-redux'
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import ErrorPage from "./pages/ErrorPage";
import HelpPage from "./pages/HelpPage";
import DoneDetail from "./features/todo/components/done/DoneDetail";
import TodoListPage from "./pages/TodoListPage";
import DoneListPage from "./pages/DoneListPage";

const root = ReactDOM.createRoot(document.getElementById('root'));

const router = createBrowserRouter([
        {
            path: "/",
            element: <App/>,
            errorElement: <ErrorPage/>,
            children:[
                {
                    index:true,
                    element:<TodoListPage/>
                },
                {
                    path: "/help",
                    element: <HelpPage/>
                },
                {
                    path:"/done/:id",
                    element:<DoneDetail/>
                },
                {
                    path: "/done",
                    element: <DoneListPage/>
                },
            ]
        },

    ]
)

root.render(
    <React.StrictMode>
        <Provider store={store}>
            <RouterProvider router={router}/>
        </Provider>
    </React.StrictMode>
);
