import DoneList from "../features/todo/components/done/DoneList";

export default function DoneListPage() {
    return (
        <div className='done-list'>
            <DoneList/>
        </div>
    );
}